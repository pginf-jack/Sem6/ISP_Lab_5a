# ISP_Lab_5a - IP Core Generator (v1)

An RS232-based module printing received input as ASCII art:

```
   11     2222    3333      44   555555    666   777777
  111    22  22  33  33    444   55       66     77  77
 1111        22      33   4444   55      66          77
   11        22      33  44 44   55      66         77
   11       22    3333  44  44   55555   66666      77
   11      22        33 4444444      55  66  66    77
   11     22         33     44       55  66  66    77
   11    22  22  33  33     44   55  55  66  66    77
  1111   222222   3333     4444   5555    6666     77
```

A single ASCII image is generated every 18 characters or when Enter is received, whichever happens first.

Input is buffered up to 64 characters. Overflow is signaled with LD0 LED.

RS232 transmission operates with the following transmission parameters:

- 9600bps
- 8 data bits
- 1 stop bit
- no parity bit

## How to simulate?

The circuit can be simulated using Vivado:

### Simulation using Vivado

Tested with: Vivado 2018.3

The project requires Nexys A7 board files that are part of [Digilent's Vivado Board Files](https://github.com/Digilent/vivado-boards).

A Vivado project can be bootstrapped using the `build.tcl` script in the root of the repository:

1. Start Vivado.
1. Open Tcl Console (Window->Tcl Console)
1. Run commands:

   ```
   cd /path/to/repository
   source build.tcl
   ```

1. The project should generate shortly after which you can start the simulation
   through "Simulation->Run Simulation->Run Behavioral Simulation" option on the left.
