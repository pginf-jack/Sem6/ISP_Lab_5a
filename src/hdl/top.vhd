library IEEE;
use IEEE.numeric_std.all;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity top is
port (
    clk_i : in STD_LOGIC;
    RXD_i : in STD_LOGIC;
    TXD_o : out STD_LOGIC;
    ld0 : out STD_LOGIC;
    led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
    led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
);
end top;

architecture data_flow of top is
    component fifo_mem
        port (
            clk : in STD_LOGIC;
            din : in STD_LOGIC_VECTOR (7 downto 0);
            wr_en : in STD_LOGIC;
            rd_en : in STD_LOGIC;
            dout : out STD_LOGIC_VECTOR (7 downto 0);
            full : out STD_LOGIC;
            empty : out STD_LOGIC
        );
    end component;
    component rs232_monitor
        port (
            clk_i : in STD_LOGIC;
            rst_i : in STD_LOGIC;
            RXD_i : in STD_LOGIC;
            data_o : out STD_LOGIC_VECTOR (7 downto 0);
            new_data_available_o : out STD_LOGIC
        );
    end component;
    component display
        port (
            signal clk_i : in STD_LOGIC;
            signal rst_i : in STD_LOGIC;
            signal digit_i : in STD_LOGIC_VECTOR (31 downto 0);
            signal led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
            signal led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component;
    component core_generator is
        port (
            clk_i : in STD_LOGIC;
            data_i : in STD_LOGIC_VECTOR (7 downto 0);
            enabled : in STD_LOGIC;
            TXD_o : out STD_LOGIC;
            ready : out STD_LOGIC
        );
    end component;

    function byteToLedHex(value : STD_LOGIC_VECTOR (3 downto 0)) return STD_LOGIC_VECTOR is
    begin
        case value is
            when "0000" => return "0000001";
            when "0001" => return "1001111";
            when "0010" => return "0010010";
            when "0011" => return "0000110";
            when "0100" => return "1001100";
            when "0101" => return "0100100";
            when "0110" => return "0100000";
            when "0111" => return "0001111";
            when "1000" => return "0000000";
            when "1001" => return "0000100";
            when "1010" => return "0001000";
            when "1011" => return "1100000";
            when "1100" => return "0110001";
            when "1101" => return "1000010";
            when "1110" => return "0110000";
            when "1111" => return "0111000";
            when others => return "1111111";
        end case;
    end function;

    signal digit_i : STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
    signal fifo_i : STD_LOGIC_VECTOR (7 downto 0);
    signal fifo_wr_en : STD_LOGIC := '0';
    signal fifo_rd_en : STD_LOGIC := '0';
    signal fifo_o : STD_LOGIC_VECTOR (7 downto 0);
    signal fifo_empty : STD_LOGIC := '0';
    signal rs232_reset : STD_LOGIC := '1';
    signal data_o : STD_LOGIC_VECTOR (7 downto 0);
    signal new_data_available_o : STD_LOGIC;
    signal core_enabled: STD_LOGIC := '0';
    signal core_ready: STD_LOGIC;

    constant WIDTH : Integer := 18;
begin
    display_0: display port map (
        clk_i => clk_i,
        rst_i => '0',
        digit_i => digit_i,
        led7_an_o => led7_an_o,
        led7_seg_o => led7_seg_o
    );
    fifo_mem_0: fifo_mem port map (
        clk => clk_i,
        din => fifo_i,
        wr_en => fifo_wr_en,
        rd_en => fifo_rd_en,
        dout => fifo_o,
        full => ld0,
        empty => fifo_empty
    );
    rs232_monitor_0: rs232_monitor port map (
        clk_i => clk_i,
        rst_i => rs232_reset,
        RXD_i => RXD_i,
        data_o => data_o,
        new_data_available_o => new_data_available_o
    );
    core_generator_0: core_generator port map (
        clk_i => clk_i,
        data_i => fifo_o,
        enabled => core_enabled,
        TXD_o => TXD_o,
        ready => core_ready
    );

    digit_i <= (
        15 downto 9 => byteToLedHex(data_o(7 downto 4)),
        7 downto 1 => byteToLedHex(data_o(3 downto 0)),
        others => '1'
    );

    process (clk_i) is
        type read_state_type is (
            waiting_for_data,  -- waiting_for_core_generator_to_finish
            waiting_for_rising_clock_for_fifo,
            waiting_for_fifo_output
        );

        variable read_state: read_state_type := waiting_for_data;
        variable skip_data_read : STD_LOGIC := '0';
    begin
        if rising_edge(clk_i) then
            rs232_reset <= '0';
            fifo_rd_en <= '0';
            fifo_wr_en <= '0';
            core_enabled <= '0';

            -- Interaction with FIFO - writing data from RS232
            if skip_data_read = '1' then
                skip_data_read := '0';
            elsif new_data_available_o = '1' then
                skip_data_read := '1';
                rs232_reset <= '1';
                fifo_wr_en <= '1';
                fifo_i <= data_o;
            end if;

            -- Interaction with core generator and FIFO
            if read_state = waiting_for_fifo_output then
                -- data from FIFO is ready, enable core generator
                core_enabled <= '1';
                read_state := waiting_for_data;
            elsif read_state = waiting_for_rising_clock_for_fifo then
                -- first rising edge since data request, read latency starts now
                read_state := waiting_for_fifo_output;
            elsif core_ready = '1' and fifo_empty = '0' then
                -- ask FIFO for data
                fifo_rd_en <= '1';
                read_state := waiting_for_rising_clock_for_fifo;
            end if;
        end if;
    end process;        
end data_flow;
