library IEEE;
use IEEE.std_logic_1164.all;

entity display is
port (
    clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    digit_i : in STD_LOGIC_VECTOR (31 downto 0);
    led7_an_o : out STD_LOGIC_VECTOR (3 downto 0) := "0111";
    led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0) := (others => '1')
);
end display;

architecture data_flow of display is
    constant FREQUENCY_DIVIDER : Integer := 100000;
begin
    with led7_an_o select
        led7_seg_o <=
            digit_i(31 downto 24) when "0111",
            digit_i(23 downto 16) when "1011",
            digit_i(15 downto 8) when "1101",
            digit_i(7 downto 0) when "1110",
            "11111111" when others;

    process (clk_i, rst_i) is
        variable iteration : Integer := 1;
    begin
        if rst_i = '1' then
            led7_an_o <= "0000";
        elsif rising_edge(clk_i) then
            if iteration < FREQUENCY_DIVIDER then
                iteration := iteration + 1;
            else
                iteration := 1;
                case led7_an_o is
                    when "0111" => led7_an_o <= "1011";
                    when "1011" => led7_an_o <= "1101";
                    when "1101" => led7_an_o <= "1110";
                    when "1110" => led7_an_o <= "0111";
                    when others => led7_an_o <= "0111";
                end case;
            end if;
        end if;
    end process;
end architecture data_flow;
