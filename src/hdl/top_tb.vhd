library IEEE;
use IEEE.std_logic_1164.all;

--  A testbench has no ports.
entity top_tb is
end top_tb;

architecture behav of top_tb is
  component top is
    port (
      clk_i : in STD_LOGIC;
      RXD_i : in STD_LOGIC;
      TXD_o : out STD_LOGIC;
      ld0 : out STD_LOGIC;
      led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
      led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
    );
  end component;

  --  Specifies which entity is bound with the component.
  for top_0: top use entity work.top;
  signal clk_i : STD_LOGIC := '1';
  signal RXD_i : STD_LOGIC := '1';
  signal TXD_o : STD_LOGIC;
  signal ld0 : STD_LOGIC;
  signal led7_an_o : STD_LOGIC_VECTOR (3 downto 0);
  signal led7_seg_o : STD_LOGIC_VECTOR (7 downto 0);

  constant PERIOD : time := 10 ns;
  constant NOMINAL_BAUD_RATE : time := 1 sec / 9600;
begin
  --  Component instantiation.
  top_0: top port map (
    clk_i => clk_i,
    RXD_i => RXD_i,
    TXD_o => TXD_o,
    ld0 => ld0,
    led7_an_o => led7_an_o,
    led7_seg_o => led7_seg_o
  );

  clk_i <= not clk_i after PERIOD/2;

  process
    --  The patterns to apply.
    type pattern_array is array (natural range <>) of STD_LOGIC_VECTOR (7 downto 0);
    -- `X` means no digit, i.e. blank display (excluding dots)
    constant patterns: pattern_array := (
      X"61",
      X"0D"
    );
    variable baud_rate : time;
    variable value : STD_LOGIC_VECTOR (7 downto 0);
  begin
    wait for NOMINAL_BAUD_RATE;  -- allow RXD_i to be '1' for some time

    for i in patterns'range loop
      value := patterns(i);
      -- send start bit
      RXD_i <= '0';
      wait for NOMINAL_BAUD_RATE;
      -- transmission started, send bits
      for bit_index in 0 to value'length-1 loop
        RXD_i <= value(bit_index);
        wait for NOMINAL_BAUD_RATE;
      end loop;
      -- send stop bit
      RXD_i <= '1';
      wait for NOMINAL_BAUD_RATE;
      -- transmission stopped
      wait for 4 ms;  -- wait for whole display to refresh
    end loop;
    wait;
  end process;
end behav;
