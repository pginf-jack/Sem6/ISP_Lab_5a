library IEEE;
use IEEE.std_logic_1164.all;

entity rs232_monitor is
port (
    clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    RXD_i : in STD_LOGIC;
    data_o : out STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
    new_data_available_o : out STD_LOGIC := '0'
);
end rs232_monitor;

architecture data_flow of rs232_monitor is
    constant TICKS_PER_SECOND : Integer := 100000000;
    constant BAUD_RATE : Integer := 9600;
    constant INCREMENTOR : Integer := 3;
    constant LIMIT : Integer := TICKS_PER_SECOND * INCREMENTOR;
    constant RANGE_SIZE : Integer := LIMIT / BAUD_RATE;
    constant HALF_RANGE_SIZE : Integer := RANGE_SIZE / 2;
begin
    process (clk_i, rst_i) is
        type state_type is (
            no_transmission,  -- waiting_for_start_bit
            waiting_for_transmission_start,
            waiting_for_next_bit_start,
            waiting_for_stable_bit,
            waiting_for_stop_bit,
            waiting_for_transmission_stop
        );
        variable iteration : Integer := 0;
        variable next_trigger : Integer := HALF_RANGE_SIZE;
        variable state : state_type := no_transmission;
        variable bit_index : Integer := 0;
        variable pending_data_o : STD_LOGIC_VECTOR (7 downto 0);
    begin
        if rst_i = '1' then
            new_data_available_o <= '0';
            state := no_transmission;
            pending_data_o := (others => '0');
        elsif rising_edge(clk_i) then
            if state = no_transmission then
                if RXD_i = '0' then
                    state := waiting_for_transmission_start;
                end if;
                iteration := 0;
                bit_index := 0;
                next_trigger := HALF_RANGE_SIZE;
            elsif state = waiting_for_stop_bit then
                if RXD_i = '1' then
                    state := waiting_for_transmission_stop;
                end if;
                iteration := 0;
                next_trigger := HALF_RANGE_SIZE;
            else
                iteration := iteration + INCREMENTOR;
            end if;

            if iteration >= next_trigger then
                if state = waiting_for_transmission_start then
                    if RXD_i = '0' then
                        state := waiting_for_next_bit_start;
                    else
                        -- false start
                        state := no_transmission;
                    end if;
                elsif state = waiting_for_next_bit_start then
                    if bit_index /= data_o'length then
                        -- beginning of a bit, wait until we're at half length
                        state := waiting_for_stable_bit;
                    else
                        -- transmission complete
                        if RXD_i = '1' then
                            -- stop bit started, wait until we're at half length
                            state := waiting_for_transmission_stop;
                        else
                            -- wait for stop bit, this happens outside this loop
                            state := waiting_for_stop_bit;
                        end if;
                    end if;
                elsif state = waiting_for_stable_bit then
                    -- we're at half length of a bit, we can store it now
                    pending_data_o(bit_index) := RXD_i;
                    bit_index := bit_index + 1;
                    state := waiting_for_next_bit_start;
                elsif state = waiting_for_transmission_stop then
                    -- we're at half length of the stop bit
                    if RXD_i = '1' then
                        -- no framing error, copy data to output
                        data_o <= pending_data_o;
                        new_data_available_o <= '1';
                    end if;
                    state := no_transmission;
                end if;

                if iteration = LIMIT then
                    iteration := 0;
                    next_trigger := HALF_RANGE_SIZE;
                else
                    next_trigger := next_trigger + HALF_RANGE_SIZE;
                end if;
            end if;
        end if;
    end process;
end architecture data_flow;
