library IEEE;
use IEEE.std_logic_1164.all;

entity rs232_write is
	port (
		clk_i : in STD_LOGIC;
		data_i : in STD_LOGIC_VECTOR (7 downto 0);
		send : in STD_LOGIC;
		TXD_o : out STD_LOGIC;
		send_ack : out STD_LOGIC
    );
end rs232_write;

architecture data_flow of rs232_write is
    constant BIT_DURATION : Integer := 10418;
begin
    process (clk_i) is
        type state_type is (
            idle,
            busy  -- writing
        );
        variable current_data : STD_LOGIC_VECTOR (7 downto 0);
        variable state : state_type := idle;
        variable iteration : Integer := BIT_DURATION;
        variable bit_index : Integer := -1;
        variable current : STD_LOGIC := '1';
    begin
        TXD_o <= current;
        if rising_edge(clk_i) then
            if send = '0' then
                send_ack <= '0';
            end if;

            if state = idle and send = '1' and send_ack = '0' then
                current_data := data_i;
                state := busy;
                send_ack <= '1';
            end if;

            if state = busy then
                if iteration < BIT_DURATION then
                    iteration := iteration + 1;
                else
                    iteration := 0;

                    if bit_index = -1 then
                        current := '0';
                        bit_index := bit_index + 1;
                    elsif bit_index < 8 then
                        current := current_data(bit_index);
                        bit_index := bit_index + 1;
                    elsif bit_index = 8 then
                        current := '1';
                        bit_index := bit_index + 1;
                    else
                        state := idle;
                        bit_index := -1;
                        iteration := BIT_DURATION;
                    end if;
                end if;
            end if;
        end if;
    end process;
end architecture data_flow;
