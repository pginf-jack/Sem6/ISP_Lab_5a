library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity core_generator is
    port (
        clk_i : in std_logic;
        data_i : in std_logic_vector (7 downto 0);
        enabled : in std_logic;
        TXD_o : out std_logic;
        ready : out std_logic
    );
end core_generator;

architecture data_flow of core_generator is
    component char_mem
        port (
            clka : in STD_LOGIC;
            addra : in STD_LOGIC_VECTOR (11 downto 0);
            douta : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component;
    component rs232_write is
        port (
            clk_i : in STD_LOGIC;
            data_i : in STD_LOGIC_VECTOR (7 downto 0);
            send : in STD_LOGIC;
            TXD_o : out STD_LOGIC;
            send_ack : out STD_LOGIC
        );
    end component;

    -- port-mapped signals
    signal rom_addr : std_logic_vector (11 downto 0) := "111111111111";
    signal rom_data : std_logic_vector (7 downto 0);
    signal rs232_data_i : std_logic_vector (7 downto 0);
    signal send : std_logic := '0';
    signal send_ack : std_logic;

    -- this component's signals
    constant WIDTH : Integer := 18;
    type character_buffer is array (WIDTH-1 downto 0) of std_logic_vector (7 downto 0);
    -- characters(n) -> ASCII code of nth character in the buffer
    signal characters : character_buffer;
    -- current line number
    signal line_number : Integer := 0;  -- values: 0-16
    -- current character
    signal current_char : Integer := 0;  -- values: 0-(buffer_index+1)
begin
    char_mem_0: char_mem port map (
        clka => clk_i,
        addra => rom_addr,
        douta => rom_data
    );
    rs232_write_0: rs232_write port map (
        clk_i => clk_i, 
        data_i => rs232_data_i,
        send => send,
        TXD_o => TXD_o,
        send_ack => send_ack
    );

    process (clk_i) is
        type state_type is (
            filling_buffer,
            waiting_for_rom,
            writing_serial
        );
        -- Each character has 8 bits indicating if certain column should be
        -- that character or a space. This is the column index of the current character.
        variable current_char_column : Integer := 0;
        -- when filling buffer, index of the current character in the buffer (up to WIDTH-1)
        -- when writing serial, length of the buffer (up to WIDTH)
        variable buffer_index : Integer := 0;
        variable state : state_type := filling_buffer;
    begin
        rom_addr(3 downto 0) <= conv_std_logic_vector(line_number, 4);
        rom_addr(11 downto 4) <= characters(current_char);
        if rising_edge(clk_i) then
            if state = filling_buffer then
                ready <= '1';
            else
                ready <= '0';
            end if;
            if send_ack = '1' then
                send <= '0';
            end if;

            if state = filling_buffer then
                if enabled = '1' then
                    if data_i = std_logic_vector(to_unsigned(13, 8)) then
                        -- print before buffer fill if CR (13) received
                        if buffer_index > 0 then
                            state := writing_serial;
                            line_number <= 0;
                            current_char <= 0;
                            current_char_column := 0;
                        end if;
                    else
                        -- add input to the buffer
                        characters(buffer_index)(7 downto 0) <= data_i;
                        buffer_index := buffer_index + 1;
                    end if;
                end if;
                if buffer_index >= WIDTH then
                    -- buffer filled, start writing
                    state := writing_serial;
                    line_number <= 0;
                    current_char <= 0;
                    current_char_column := 0;
                end if;

            elsif state = waiting_for_rom then
                -- first rising edge since data request, read latency starts now
                -- and will be ready on next rising edge
                state := writing_serial;

            -- state = writing_serial
            elsif line_number = 16 then
                -- all characters in the buffer printed, we can fill buffer again
                state := filling_buffer;
                buffer_index := 0;
            elsif send_ack = '1' then
                -- don't do anything while there's an active send ACK
                -- This is set after our send <= '1' gets acknowledged *and*
                -- it remains set until our send <= '0' is seen by RS 232 write module.
            elsif send = '1' then
                -- don't do anything, we need to wait for rs232 to ACK send
            elsif current_char = buffer_index then
                -- buffer length (buffer_index) reached, print CR (13)
                -- and increment current_char to send LF (10) later
                rs232_data_i <= std_logic_vector(to_unsigned(13, rs232_data_i'length));
                send <= '1';
                current_char <= current_char + 1;
            elsif current_char = buffer_index + 1 then
                -- send second new line character and start printing next column
                rs232_data_i <= std_logic_vector(to_unsigned(10, rs232_data_i'length));
                send <= '1';
                current_char <= 0;
                line_number <= line_number + 1;
            else
                case rom_data(7 - current_char_column) is
                    when '1' =>
                        if
                            CONV_INTEGER(characters(current_char)) >= 32
                            and CONV_INTEGER(characters(current_char)) <= 127
                        then
                            rs232_data_i <= characters(current_char);
                        else
                            rs232_data_i <= X"2A";  -- '*'
                        end if;
                    when '0' => rs232_data_i <= X"20";  -- ' '
                    when others => rs232_data_i <= X"20";
                end case;
                send <= '1';

                if current_char_column = 7 then
                    current_char_column := 0;
                    current_char <= current_char + 1;
                else    
                    current_char_column := current_char_column + 1;
                end if;
            end if;
        end if;
    end process;
end architecture data_flow;
